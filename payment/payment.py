from flask import (
    Blueprint, flash, redirect, render_template, request, url_for
)
import hashlib
import json
import requests
import datetime
from payment import db
from payment.models import Order
from flask import current_app

bp = Blueprint('payment', __name__)
secretKey = 'SecretKey01'

def create_order(r):
    db.session.add(Order(amount=r.form['amount'], time=datetime.datetime.now(), currency=r.form['currency'], description=r.form['description'], order_id=r.form['shop_order_id']))
    db.session.commit()

@bp.route('/', methods=('GET', 'POST'))
def index():
    if request.method == 'POST':
        currency = request.form['currency']
        if currency == '978':
            current_app.logger.info('Post request for pay method')
            keys = ['amount', 'currency', 'shop_id', 'shop_order_id']
            dataSign = ':'.join([request.form[i] for i in keys]) + secretKey
            sign = hashlib.sha256(dataSign.encode('utf-8')).hexdigest()

            create_order(request)

            return json.dumps({"sign": sign})
        elif currency == '840':
            current_app.logger.info('Post request for bill method')
            keys = [request.form['currency'],
                request.form['amount'],
                request.form['currency'],
                request.form['shop_id'],
                request.form['shop_order_id']]
            dataSign = ':'.join(keys) + secretKey
            sign = hashlib.sha256(dataSign.encode('utf-8')).hexdigest()
            reqDict = {"description": request.form['description'],
                "payer_currency": request.form['currency'],
                "shop_amount": request.form['amount'],
                "shop_currency": request.form['currency'],
                "shop_id": request.form['shop_id'],
                "shop_order_id": request.form['shop_order_id'],
                "sign": sign}
            res = requests.post('https://core.piastrix.com/bill/create', json=reqDict)
            resDict = res.json()
            
            current_app.logger.info('Bill creating response ---> %s', resDict)
            create_order(request)

            if resDict['data']:
                return redirect(resDict['data']['url'])
            else:
                return resDict
        else:
            current_app.logger.info('Post request for invoice method')
            keys = [request.form['amount'],
                request.form['currency'],
                'payeer_rub',
                request.form['shop_id'],
                request.form['shop_order_id']]
            dataSign = ':'.join(keys) + secretKey
            sign = hashlib.sha256(dataSign.encode('utf-8')).hexdigest()
            reqDict = {"description": request.form['description'],
                "amount": request.form['amount'],
                "currency": request.form['currency'],
                "shop_id": request.form['shop_id'],
                "payway": 'payeer_rub',
                "shop_order_id": request.form['shop_order_id'],
                "sign": sign}
            res = requests.post('https://core.piastrix.com/invoice/create', json=reqDict)
            resDict = res.json()
            
            current_app.logger.info('Invoice creating response ---> %s', resDict)
            create_order(request)

            if resDict['data']:
                return redirect(resDict['data']['data']['referer'])
            else:
                return resDict

    orders = Order.query.all()
    return render_template('index.html', orders=orders)
