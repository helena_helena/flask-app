let payForm = document.getElementById('payForm');

function submitForm(e) {
    let currency = document.getElementById('currency');
    let sign = document.getElementById('sign');
    let isEUR = currency.value === '978';

    payForm.action = isEUR ? 'https://pay.piastrix.com/ru/pay' : '/';
    if (!isEUR) return; // Default POST if not EUR
    
    e.preventDefault();
    fetch('/', {
        method: 'POST',
        body: new FormData(payForm)
    })
    .then(response => response.json())
    .then(data => {
        sign.value = data.sign;
        payForm.action = 'https://pay.piastrix.com/ru/pay'
        payForm.submit();
    });
}

payForm.addEventListener('submit', submitForm);
