from payment import db

class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Float(), nullable=False)
    currency = db.Column(db.Integer(), nullable=False)
    description = db.Column(db.Text(), nullable=False)
    order_id = db.Column(db.Text(), nullable=False)
    time = db.Column(db.DateTime(), nullable=False)

    def __repr__(self):
        return '<Order: {}>'.format(self.name)